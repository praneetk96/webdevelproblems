# webDevelProblems

Here i'll be uploading and maintaining my completed assignments as a "JavaScript Trainee Developer" at XYZ company.

## Description

The main purpose of this repo is to help any beginner to understand how a static and dynamic website works. After working as a "JavaScript Trainee Developer", i thought of sharing my understanding of a problem.

[![license](https://img.shields.io/badge/license-GNU%20AGPLv3-success)](https://www.gnu.org/licenses/agpl-3.0.html)

## Screenshots & Links

**Will upload when all the files are uploaded which will take time.**

## License

The files in this repository named **webDevelProblems** is licensed under the GNU AFFERO GENERAL PUBLIC LICENSE.

## Project status

*Under Development.*
