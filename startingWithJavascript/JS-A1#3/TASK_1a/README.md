# JS-A1#3

## Task 1(a)

Write a Javascript code that use a for loop to print the numbers from 0 to 3 on the console.  

## Output

| File name        | Screenshots                                                                                                                                                            |
|:----------------:|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| ***index.html*** | <img title="JS-A1#3-1a" src="https://gitlab.com/praneetk96/webdevelproblems/-/raw/main/src/images/JS-A1%233/Task_1a/index.png" alt="JS-A1#3-1a" data-align="center">   |
| ***Task-1a.js*** | <img title="JS-A1#3-1a" src="https://gitlab.com/praneetk96/webdevelproblems/-/raw/main/src/images/JS-A1%233/Task_1a/Task_1a.png" alt="JS-A1#3-1a" data-align="center"> |
| ***output.png*** | <img title="JS-A1#3-1a" src="https://gitlab.com/praneetk96/webdevelproblems/-/raw/main/src/images/JS-A1%233/Task_1a/output.png" alt="JS-A1#3-1a" data-align="center">  |