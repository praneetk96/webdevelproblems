# JS-A1#1

## Task 1

1. Make a file hello.html and use it to print Hello World and Welcome to Javascript on the console.

2. Using separation of concerns, create a file file1.js and put the Javascript code in it. Use the file1.js in hello.html.

3. Add a comment to file.js

## Output

| **File Name**    | **Output**                                                                                                                                                       |
|:----------------:|:----------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| ***hello.html*** | <img title="JS-A1#1-1" src="https://gitlab.com/praneetk96/webdevelproblems/-/raw/main/src/images/JS-A1%231/JS-A1%231-1.png" alt="JS-A1#1-1" data-align="center"> |
| ***file1.js***   | <img title="JS-A1#1-2" src="https://gitlab.com/praneetk96/webdevelproblems/-/raw/main/src/images/JS-A1%231/JS-A1%231-2.png" alt="JS-A1#1-2" data-align="center"> |
