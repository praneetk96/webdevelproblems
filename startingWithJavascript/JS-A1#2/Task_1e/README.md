# JS-A1#2

## Task 1(e)

What is a constant. How is a constant different from a variable.  

## Output

Constant is a variable whose value cannot be changed after declaring it, constant variable can be declared as, eg:

```		
const fontSize = 14; //where, const is the keyword, cannot be altered
```  

There are differences between Const and variable:

- Const variable cannot be reassigned while a Variable can be.
- Const variable cannot be redeclared while a Variable can be.
- Const variable have block scope