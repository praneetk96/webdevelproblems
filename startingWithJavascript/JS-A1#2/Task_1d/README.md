# JS-A1#2

## Task 1(d)

What is a variable. What should be kept in mind while naming a variable.  

## Output

Variables are used to store data and information, variables can be declared by using 
some predefined keyword such as:

- var
- let

'var' variables was used to declare earlier but 'let' variable is preffered to 
be used. There are few rules that are needed to keep in mind such as:

- A variable type 'let' value can be reassigned but declared variable cannot be declared again.
- A variable name cannot start with number.
- Whitespaces are not allowed in a variable.
- Variable names in javascript is case-sensitive.