# JS-A1#2

## Task 1(a)

Write a Javascript code that adds two numbers and prints the output on the console.

## Output

| File Name        | Output                                                                                                                                                              |
|:----------------:|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| ***index.html*** | <img title="JS-A1#1-1" src="https://gitlab.com/praneetk96/webdevelproblems/-/raw/main/src/images/JS-A1%232/Task_1a/index.png" alt="JS-A1#1-1" data-align="center">  |
| ***add.js***     | <img title="JS-A1#1-1" src="https://gitlab.com/praneetk96/webdevelproblems/-/raw/main/src/images/JS-A1%232/Task_1a/add.png" alt="JS-A1#1-1" data-align="center">    |
| ***output***     | <img title="JS-A1#1-1" src="https://gitlab.com/praneetk96/webdevelproblems/-/raw/main/src/images/JS-A1%232/Task_1a/output.png" alt="JS-A1#1-1" data-align="center"> |
