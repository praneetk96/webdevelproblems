# JS-A1#2

## Task 1(c)

Write the Javascript code that given name and age makes a sentence with the name and age and prints it on the console.  

## Output

| File name         | Screenshots                                                                                                                                                           |
|:-----------------:|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| ***index.html***  | <img title="JS-A1#2-c" src="https://gitlab.com/praneetk96/webdevelproblems/-/raw/main/src/images/JS-A1%232/Task_1c/index.png" alt="JS-A1#2-c" data-align="center">    |
| ***name&Age.js*** | <img title="JS-A1#2-c" src="https://gitlab.com/praneetk96/webdevelproblems/-/raw/main/src/images/JS-A1%232/Task_1c/name&Age.png" alt="JS-A1#2-c" data-align="center"> |
| ***output.png***  | <img title="JS-A1#2-c" src="https://gitlab.com/praneetk96/webdevelproblems/-/raw/main/src/images/JS-A1%232/Task_1c/output.png" alt="JS-A1#2-c" data-align="center">   |