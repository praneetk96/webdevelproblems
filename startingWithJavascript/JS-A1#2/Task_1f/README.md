# JS-A1#2

## Task 1(f)

What are primitive data types. Name all of them with examples.  

## Output

There are two types of datatypes in javascript, Primitive and Reference types. The major difference between them is that Primitive datatypes are immutable.

The datatypes in Primitive datatypes are:

- Number
- String
- Boolean
- Undefined
- Null