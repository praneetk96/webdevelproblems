# JS-A1#2

## Task 3  

Write a Javascript code where 3 numbers a, b and c are given. Write appropriate code to create a string similar to the one shown below and print it on the console "a + b +  c = d". Note that a, b, c and d should be replaced by their appropriate values.  

## Output  

| File name        | Screenshots                                                                                                                                                        |
|:----------------:|:------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| ***index.html*** | <img title="JS-A1#2-3" src="https://gitlab.com/praneetk96/webdevelproblems/-/raw/main/src/images/JS-A1%232/Task_3/index.png" alt="JS-A1#2-3" data-align="center">  |
| ***Task-3.js***  | <img title="JS-A1#2-3" src="https://gitlab.com/praneetk96/webdevelproblems/-/raw/main/src/images/JS-A1%232/Task_3/Task_3.png" alt="JS-A1#2-3" data-align="center"> |
| ***output.png*** | <img title="JS-A1#2-3" src="https://gitlab.com/praneetk96/webdevelproblems/-/raw/main/src/images/JS-A1%232/Task_3/output.png" alt="JS-A1#2-3" data-align="center"> |

