# JS-A1#2

## Task 1(b)

Write a Javascript code that concatenates two stringsand prints the output on the console.

## Output

| File Name        | Output                                                                                                                                                               |
|:----------------:|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| ***index.html*** | <img title="JS-A1#2-b" src="https://gitlab.com/praneetk96/webdevelproblems/-/raw/main/src/images/JS-A1%232/Task_1b/index.png" alt="JS-A1#2-b" data-align="center">   |
| ***concate.js*** | <img title="JS-A1#2-b" src="https://gitlab.com/praneetk96/webdevelproblems/-/raw/main/src/images/JS-A1%232/Task_1b/concate.png" alt="JS-A1#2-b" data-align="center"> |
| ***output***     | <img title="JS-A1#2-b" src="https://gitlab.com/praneetk96/webdevelproblems/-/raw/main/src/images/JS-A1%232/Task_1b/output.png" alt="JS-A1#2-b" data-align="center">  |
