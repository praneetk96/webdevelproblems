# JS-A1#2

## Task 2

Write a Javascript code where 2 numbers a and b are given. Add the numbers and print the sum on the console. Also create a string "The sum of a and b is c" where a, band c should be the actual values of the numbers. Print it on the console.  

## Output

| File name        | Screenshots                                                                                                                                                        |
|:----------------:|:------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| ***index.html*** | <img title="JS-A1#2-2" src="https://gitlab.com/praneetk96/webdevelproblems/-/raw/main/src/images/JS-A1%232/Task_2/index.png" alt="JS-A1#2-2" data-align="center">  |
| ***Task-2.js***  | <img title="JS-A1#2-2" src="https://gitlab.com/praneetk96/webdevelproblems/-/raw/main/src/images/JS-A1%232/Task_2/Task_2.png" alt="JS-A1#2-2" data-align="center"> |
| ***output.png*** | <img title="JS-A1#2-2" src="https://gitlab.com/praneetk96/webdevelproblems/-/raw/main/src/images/JS-A1%232/Task_2/output.png" alt="JS-A1#2-2" data-align="center"> |